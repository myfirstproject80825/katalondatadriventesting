<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>loginPage</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>a23808ce-c2fd-45a1-93a1-84706fed2e77</testSuiteGuid>
   <testCaseLink>
      <guid>00cab3cc-a2c6-44cf-9795-8dd9bec7027e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/loginPage/loginInvalidUsernameAndInvalidPassword</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>10aec41f-4391-4fbf-adfc-1f5fb2ee6429</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <iterationNameVariable>
         <defaultValue>findTestData('testData')</defaultValue>
         <description></description>
         <id>ee664945-9a13-4795-a0a5-0b1fc7de2f81</id>
         <masked>false</masked>
         <name>password</name>
      </iterationNameVariable>
      <testCaseId>Test Cases/loginPage/loginInvalidUsernameAndValidPassword</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>f5fbcae3-db90-4f43-954a-f55a24fa306a</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/testData</testDataId>
      </testDataLink>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
      <variableLink>
         <testDataLinkId>f5fbcae3-db90-4f43-954a-f55a24fa306a</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>username</value>
         <variableId>0fd7f34c-d570-447d-9d8e-49b3863cd4bb</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>f5fbcae3-db90-4f43-954a-f55a24fa306a</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>password</value>
         <variableId>ee664945-9a13-4795-a0a5-0b1fc7de2f81</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>644eb080-b0a5-44f4-8f6a-1c475bcbd290</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/loginPage/loginValidUsernameAndInvalidPassword</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>9c31fb44-b54c-43b7-aac4-89bba571c4a7</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/loginPage/loginValidUsernameAndPassword</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>fe0301ff-14bc-4bc9-b82f-0f5f1eb44a7b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/loginPage/loginWithoutAnything</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>74c7ff47-a0ae-4294-9f56-6d6922e8a4f5</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/testData</testDataId>
      </testDataLink>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
</TestSuiteEntity>
